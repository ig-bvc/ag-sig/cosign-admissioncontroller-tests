# CoSign Tests OKD und OpenShift 4

Szenarien:
  
  - Cosign 2.0.1
  - cosigned Admission Controller
  - GateKeeper und Cosign gatekeeper provider
  - Connaisseur

Diese Config sollte auf OKD 4.11+ / OpenShift 4.11+ laufen.

## CoSign 1.5.1

- Installation von: https://github.com/sigstore/cosign/releases/tag/v1.5.1
- Master key generieren: `cosign generate-key-pair k8s://brz-igbvc-poc/cosign-masterkey`
  - Generiert einen Masterkey und verspeichert ihn als Secret im Namespace `brz-igbc-poc` 
- Signieren eines Images:

    ```shell
    docker login reg.pflaeging.net
    cosign sign --key k8s://brz-igbvc-poc/cosign-masterkey reg.pflaeging.net/sig-poc/pflaeging-net-ubi-debug:latest
    cosign verify --key k8s://brz-igbvc-poc/cosign-masterkey reg.pflaeging.net/sig-poc/pflaeging-net-ubi-debug:latest
    ```

## Cosigned Installation und Test im Cluster

Ist ein helm Installer:

- musste gepatched werden, da Security Constraints falsch gesetz waren (zumindest für OpenShift)
- Admission Controller funktioniert nicht, da die Rechtekette (RBAC) falsch aufgebaut ist.

Wurde in dieser Version erst einmal zur Seite gelegt!

## Gatekeeper mit cosign-gatekeeper-provider

- GateKeeper Installation mit dem Gatekeeper Operator: geht und ist funktional!
- Es gibt eine Extension zu Gatekeeper: cosign-gatekeeper-provider (https://github.com/sigstore/cosign-gatekeeper-provider)
  - aus dem Readme: `This repo is meant for testing Gatekeeper external data feature. Do not use for production.``
  - das zugrundeliegende "Gatekeeper External Data Feature" ist "Early alpha" (https://open-policy-agent.github.io/gatekeeper/website/docs/externaldata/)

Weitere Tests wurden abgebrochen nach kurzem Cluster Crash!

## Connaisseur

Connaisseur ist mittlerweile in der Version 2.8.0 verfügbar:

- voller Support für OpenShift
- Support für NotaryV1 und cosign
- gute Konfigurationsmöglichkeiten
- konfigurierbar pro Namespace!

### Installation

- Dieses Repo mit Submodulen clonen
- `OKD-values.yaml` anpassen
- Login als Clusteradmin
- Rollout mit: `helm install connaisseur connaisseur/helm --atomic --create-namespace --namespace infra-connaisseur -f OKD-values.yaml`

## Tests

Setzen des Labels `securesystemsengineering.connaisseur/webhook=validate` auf dem Testnamespace:

`oc label ns/ig-bvc securesystemsengineering.connaisseur/webhook=validate` als clusteradmin ;-).

```shell
oc login -u normaluser api.mycluster.net:6443
oc apply -f tests/nosign-pod.yaml
# Sollte einen Fehler generieren
oc apply -f tests/sign-pod.yaml
# Sollte funktionieren
```

---
Peter Pfläging <<peter@pflaeging.net>>